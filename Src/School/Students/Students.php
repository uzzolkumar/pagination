<?php
namespace App\School\Students;
use pdo;
class Students
{

    public $fname = '';
    public $sname = '';
    public $id = '';
    public $email = '';
    public $phone = '';
    public $program = '';
    public $pdo='';

    public function __construct()
    {
        session_start();
       $this->pdo = new PDO('mysql:host=localhost;dbname=informations', 'root', '');
    }
    public function setData($data = '')
    {
        if (array_key_exists('fname', $data)) {
            $this->fname = $data['fname'];
        }
        if (array_key_exists('sname', $data)) {
            $this->sname = $data['sname'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
        }
        if (array_key_exists('droopDown', $data)) {
            $this->program = $data['droopDown'];
        }
        return $this;
    }
    public function Store()
    {

        try {
            $query  = "INSERT INTO students (id,Unique_id,fname,sname,email,phone,program) VALUES (:a,:p,:b,:f,:d,:e,:g)";
           $stmt =$this->pdo->prepare($query);
            $result = $stmt->execute(array(
                ':a' =>null,
                ':p'=>uniqid(),
                ':b' => $this->fname,
                ':f' => $this->sname,
                ':d' => $this->email,
                ':e' => $this->phone,
                ':g' => $this->program
            ));
            if ($result) {

                $_SESSION['messageIns'] = "Data submitted successfully";
                header("location:create.php");
            }
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function show($noOfItemperPaege=' ',$offset='')
    {
        try {
            $query = "SELECT * FROM `students` limit $noOfItemperPaege OFFSET $offset";

            $stmt = $this->pdo->prepare($query);

             $stmt->execute();

            $result = $stmt->fetchAll();
            if ($result) {

                return $result;
            }
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }

    }
    public function noOfRows()
    {
        try {
            $query = "SELECT * FROM `students`";

            $stmt = $this->pdo->prepare($query);

            $stmt->execute();

            $result = count($stmt->fetchAll());
            return $result;

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }

    }
    public function view()
    {
        try {
            $query = "SELECT * FROM `students` WHERE Unique_id='$this->id'";

            $stmt = $this->pdo->prepare($query);

            $stmt->execute();

            $result = $stmt->fetch();
        if(empty($result)){

            session_start();
            $_SESSION['message1']='Something wrong';
            header('location:index.php');
        }
        else{
            return $result;
        }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }

    }
    public function editView()
    {
        try {
            $query = "SELECT * FROM `students` WHERE Unique_id='$this->id'";

            $stmt = $this->pdo->prepare($query);

            $stmt->execute();

            $result = $stmt->fetch();

                return $result;

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }

    }
    public function delete()
    {
        try {

            $query = "Delete FROM `students` WHERE Unique_id='$this->id'";

            $stmt = $this->pdo->prepare($query);

            $result = $stmt->execute();
            if($result)
            {
                 $_SESSION['message'] ='Successfully Deleted';
                 header("location:index.php");
            }


        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function update()
    {
        try {

            $query = "UPDATE students set fname=:a,sname=:b,email=:g,phone=:d,program=:e WHERE Unique_id='$this->id'";

            $stmt =$this->pdo->prepare($query);
           $result= $stmt->execute(array(
                ':a'=>$this->fname,
                ':b'=>$this->sname,
                ':g'=>$this->email,
                ':d'=>$this->phone,
                ':e'=>$this->program,
            ));
            if($result)
            {
                $_SESSION['message1']='Successfully Updated';
                header('location:edit.php');
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }

    }

}
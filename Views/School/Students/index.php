<?php
include_once("../../../vendor/autoload.php");
use App\School\Students\Students;


$obj = new Students();
if(isset($_GET['perpage']))
{
    $noOfItemperPaege=$_GET['perpage'];
}
else{
    $noOfItemperPaege=5;
}
if(!empty($_GET['page'])){
    $currentPage = $_GET['page']-1;
}else{
    $currentPage=0;
}
 $offset = $noOfItemperPaege *$currentPage;

$totalRow= $obj->noOfRows();
$totalPage =ceil(($totalRow/$noOfItemperPaege)) ;

$result = $obj->show($noOfItemperPaege,$offset);
?>
<html>
<head>
    <title>
        Create | Page
    </title>
    <link href="../../../css/style.css" rel="stylesheet" type="text/css"/>
    <style>


        . table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        a {
            text-decoration: none;
        }
    </style>

</head>

<body>
<div class="inputfield">
    <a href="create.php">Add Student Information </a>
    <form action="" method="get">
        <span>show value in page : </span><input type="text" name="perpage"  style="width:25% "/>
        <input type="submit" value="Show" style="width:10% "/>
    </form>

    <table width="100%">
        <tr>
            <td>Serial</td>
            <td>Name</td>
            <td style="text-align:center">Action</td>

        </tr>
        <?php
        $i = $offset+1;
        if (!empty($result)) {
            foreach ($result as $key => $item) {

                ?>
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $item['fname'] . ' ' . $item['sname']; ?></td>
                    <td style="text-align:center">
                        <a href="view.php?id=<?php echo $item['Unique_id']; ?>">View </a>
                        |
                        <a href="edit.php?id=<?php echo $item['Unique_id']; ?>">Edit </a>
                        |
                        <a href="delete.php?id=<?php echo $item['Unique_id']; ?>">Delete </a>


                    </td>
                </tr>

            <?php }
        } ?>
    </table>

    <?php
for($i=1;$i<=$totalPage;$i++)
{
    ?>

    <a href="index.php?page=<?php echo $i;?>&perpage=<?php echo $noOfItemperPaege;?>"><?php echo $i;?></a>
   <?php }  ?>
    <?php

    if (isset($_SESSION['message1'])) {
        echo $_SESSION['message1'];
        unset($_SESSION['message1']);
    }

    if (isset($_SESSION['message'])) {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }

    ?>
</div>
</body>
</html>

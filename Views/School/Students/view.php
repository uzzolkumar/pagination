
<?php
include_once ("../../../vendor/autoload.php");
use App\School\Students\Students;

$obj = new Students();

$result=$obj->setData($_GET)->view();
?>
<html>
<head>
    <title>
Create | Page
</title>
    <link href="../../../css/style.css" rel="stylesheet" type="text/css" />
    <style>


        . table {
    font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
    border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
background-color: #dddddd;
        }
        a{
    text-decoration: none;
        }
    </style>

</head>

<body>
<div class="inputfield" >
    <a href="index.php">Back To Student List </a>
    <table width="100%" >
        <tr>
            <td>Id</td>
            <td>Name</td>
            <td>Email</td>
            <td>Phone</td>
            <td>Program</td>
            <td style="text-align:center">Action</td>

        </tr>
            <tr >
                <td ><?php echo $result['id'];?></td>
                <td ><?php echo $result['fname'].' '.$result['sname'];?></td>
                <td ><?php echo $result['email'];?></td>
                <td ><?php echo $result['phone'];?></td>
                <td ><?php echo $result['program'];?></td>
                <td style="text-align:center" >


                    <a href="edit.php?id=<?php echo $result['Unique_id'];?>">Edit </a>
                    |
                    <a href="delete.php?id=<?php echo $result['Unique_id'];?>">Delete </a>


                </td>
            </tr>

</table>
    <?php
    if(isset($_SESSION['message']))
    {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
    ?>
</div>
</body>
</html>
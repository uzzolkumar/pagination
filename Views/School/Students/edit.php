<?php
include_once("../../../vendor/autoload.php");
use App\School\Students\Students;


$obj = new Students();


$result = $obj->setData($_GET)->editView();

if(isset($_SESSION['message']))
{
 $error= $_SESSION['message'];
}
?>

<html>
<head>
    <title>
        Create | Page
    </title>
    <link href="../../../css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="inputfield">
    <a href="index.php">View Student List</a>
    <form action="update.php" method="post">

        <input type="hidden" name="id" value="<?php echo $result['Unique_id']; ?>"/>
        First Name : <input type="text" name="fname" value="<?php echo $result['fname']; ?>"
                            placeholder="Enter First Name"/>
        <?php if (isset($error['fname1'])) {
            echo $error['fname1'];
        }
        ?>
        <?php if (isset($error['fname2'])) {
            echo $error['fname2'];
        }
        ?>
        Second Name : <input type="text" name="sname" value="<?php echo $result['sname']; ?>"
                             placeholder="Enter Second Name"/>
        <?php if (isset($error['sname1'])) {
            echo $error['sname1'];
        }
        ?>
        <?php if (isset($error['sname2'])) {
            echo $error['sname2'];
        }
        ?>
        Email : <input type="text" name="email" value="<?php echo $result['email']; ?>" placeholder="Email"/>
        <?php if (isset($error['email1'])) {
            echo $error['email1'];
        }
        ?>
        <?php if (isset($error['email2'])) {
            echo $error['email2'];
        }
        ?>
        Phone : <input type="text" name="phone" value="<?php echo $result['phone']; ?>" placeholder="+8801"/>
        <?php if (isset($error['phone1'])) {
            echo $error['phone1'];
        }
        ?>
        <?php if (isset($error['phone2'])) {
            echo $error['phone2'];
        }
        ?>

        Program : <select name="droopDown">
            <option value="<?php echo $result['program']; ?>"> <?php echo $result['program']; ?> </option>
            <option value="0">Choose Program</option>
            <option value="CSE">CSE</option>
            <option value="EEE">EEE</option>
            <option value="CE">CE</option>
            <option value="EETE">EETE</option>
        </select> <br/>
        <?php if (isset($error['droopDown1'])) {
            echo $error['droopDown1'];
        }
        ?>
        <br/>


        <input type="submit" name="Enter" value="Update"/></br>
        <?php


        if (isset($_SESSION['message1'])) {
            echo $_SESSION['message1'];
            unset($_SESSION['message1']);
        }
        session_unset();
        ?>

    </form>
</div>
</body>
</html>

